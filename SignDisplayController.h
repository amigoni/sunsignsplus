//
//  SignDisplayController.h
//  SunSing+
//
//  Created by Leonardo Amigoni on 2/17/16.
//  Copyright © 2016 Leonardo Amigoni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignGroup.h"

@interface SignDisplayController : UIView

@property (weak, nonatomic) IBOutlet SignGroup *topLeftSquareContainer;
@property (weak, nonatomic) IBOutlet SignGroup *topRightSquareContainer;
@property (weak, nonatomic) IBOutlet SignGroup *bottomRightSquareContainer;
@property (weak, nonatomic) IBOutlet SignGroup *bottomLeftSquareContainer;

@property (weak, nonatomic) IBOutlet SignGroup *topTriangleContainer;
@property (weak, nonatomic) IBOutlet SignGroup *rightTriangleContainer;
@property (weak, nonatomic) IBOutlet SignGroup *bottomTriangleContainer;
@property (weak, nonatomic) IBOutlet SignGroup *leftTriangleContainer;

@property (weak, nonatomic) IBOutlet SignGroup *topArrowContainer;
@property (weak, nonatomic) IBOutlet SignGroup *rightArrowContainer;
@property (weak, nonatomic) IBOutlet SignGroup *bottomArrowContainer;
@property (weak, nonatomic) IBOutlet SignGroup *leftArrowContainer;

- (void)configureWithPlanetSigns: (int) sun mercury: (int) mercury venus: (int) venus mars: (int) mars;
- (void) toggleAllSymbols: (BOOL) isHidden;

@end
