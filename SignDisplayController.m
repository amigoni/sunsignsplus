//
//  SignDisplayController.m
//  SunSing+
//
//  Created by Leonardo Amigoni on 2/17/16.
//  Copyright © 2016 Leonardo Amigoni. All rights reserved.
//

#import "SignDisplayController.h"

@implementation SignDisplayController

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews {
    //Squares
    self.topLeftSquareContainer.shape = @"square";
    self.topRightSquareContainer.shape = @"square";
    self.bottomRightSquareContainer.shape = @"square";
    self.bottomLeftSquareContainer.shape = @"square";
    
    [self.topLeftSquareContainer changeColor:@"yellow"];
    [self.topRightSquareContainer changeColor:@"blue"];
    [self.bottomRightSquareContainer changeColor:@"red"];
    [self.bottomLeftSquareContainer
     changeColor:@"green"];
    
    //Triangles
    self.topTriangleContainer.shape = @"triangle";
    self.rightTriangleContainer.shape = @"triangle";
    self.bottomTriangleContainer.shape = @"triangle";
    self.leftTriangleContainer.shape = @"triangle";
    
    [self.topTriangleContainer changeColor:@"red"];
    [self.rightTriangleContainer changeColor:@"green"];
    [self.bottomTriangleContainer changeColor:@"yellow"];
    [self.leftTriangleContainer
     changeColor:@"blue"];
    
    self.rightTriangleContainer.transform = CGAffineTransformMakeRotation(M_PI_2);
    self.bottomTriangleContainer.transform = CGAffineTransformMakeRotation(M_PI);
    self.leftTriangleContainer.transform = CGAffineTransformMakeRotation(M_PI_2*3);
    
    [self.rightTriangleContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.bottomTriangleContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.leftTriangleContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSDictionary *triangles = NSDictionaryOfVariableBindings(_rightTriangleContainer, _bottomTriangleContainer,_leftTriangleContainer);
    
    NSArray *horizontalConstraintsT1 =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-240-[_rightTriangleContainer(50)]|" options:0 metrics:nil views:triangles];
    NSArray *horizontalConstraintsT2 =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-80-[_bottomTriangleContainer(50)]|" options:0 metrics:nil views:triangles];
    NSArray *horizontalConstraintsT3 =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_leftTriangleContainer(50)]|" options:0 metrics:nil views:triangles];
    NSArray *verticalConstraintsT1 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-170-[_rightTriangleContainer(60)]|"  options:0 metrics:nil views:triangles];
    NSArray *verticalConstraintsT2 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-230-[_bottomTriangleContainer(60)]|"  options:0 metrics:nil views:triangles];
    NSArray *verticalConstraintsT3 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-80-[_leftTriangleContainer(60)]|"  options:0 metrics:nil views:triangles];
    
    [self addConstraints:horizontalConstraintsT1];
    [self addConstraints:horizontalConstraintsT2];
    [self addConstraints:horizontalConstraintsT3];
    [self addConstraints:verticalConstraintsT1];
    [self addConstraints:verticalConstraintsT2];
    [self addConstraints:verticalConstraintsT3];
    
    //Arrows
    self.topArrowContainer.shape = @"arrow";
    self.rightArrowContainer.shape = @"arrow";
    self.bottomArrowContainer.shape = @"arrow";
    self.leftArrowContainer.shape = @"arrow";
    
    [self.topArrowContainer changeColor:@"green"];
    [self.rightArrowContainer changeColor:@"yellow"];
    [self.bottomArrowContainer changeColor:@"blue"];
    [self.leftArrowContainer
     changeColor:@"red"];
    
    self.rightArrowContainer.transform = CGAffineTransformMakeRotation(M_PI_2);
    self.bottomArrowContainer.transform = CGAffineTransformMakeRotation(M_PI);
    self.leftArrowContainer.transform = CGAffineTransformMakeRotation(M_PI_2*3);
    
    [self.rightArrowContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.bottomArrowContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.leftArrowContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
    NSDictionary *arrows = NSDictionaryOfVariableBindings(_rightArrowContainer, _bottomArrowContainer, _leftArrowContainer);
    NSArray *horizontalConstraintsA1 =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-204-[_rightArrowContainer(70)]|" options:0 metrics:nil views:arrows];
    NSArray *horizontalConstraintsA2 =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-150-[_bottomArrowContainer(70)]|" options:0 metrics:nil views:arrows];
    NSArray *horizontalConstraintsA3 =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-24-[_leftArrowContainer(70)]|" options:0 metrics:nil views:arrows];
    NSArray *verticalConstraintsA1 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-76-[_rightArrowContainer(112)]|"  options:0 metrics:nil views:arrows];
    NSArray *verticalConstraintsA2 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_bottomArrowContainer(112)]|"  options:0 metrics:nil views:arrows];
    NSArray *verticalConstraintsA3 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-130-[_leftArrowContainer(112)]|"  options:0 metrics:nil views:arrows];
    
    [self addConstraints:horizontalConstraintsA1];
    [self addConstraints:horizontalConstraintsA2];
    [self addConstraints:horizontalConstraintsA3];
    [self addConstraints:verticalConstraintsA1];
    [self addConstraints:verticalConstraintsA2];
    [self addConstraints:verticalConstraintsA3];
    
}

- (void)configureWithPlanetSigns: (int) sun mercury: (int) mercury venus: (int) venus mars: (int) mars {
    if (sun == 1) {
        self.leftArrowContainer.innerImage.hidden = false;
    } else if (sun == 2) {
        self.bottomLeftSquareContainer.innerImage.hidden = false;
    } else if (sun == 3) {
        self.bottomTriangleContainer.innerImage.hidden = false;
    } else if (sun == 4) {
        self.bottomArrowContainer.innerImage.hidden = false;
    } else if (sun == 5) {
        self.bottomRightSquareContainer.innerImage.hidden = false;
    } else if (sun == 6) {
        self.rightTriangleContainer.innerImage.hidden = false;
    } else if (sun == 7) {
        self.rightArrowContainer.innerImage.hidden = false;
    } else if (sun == 8) {
        self.topRightSquareContainer.innerImage.hidden = false;
    } else if (sun == 9) {
        self.topTriangleContainer.innerImage.hidden = false;
    } else if (sun == 10) {
        self.topArrowContainer.innerImage.hidden = false;
    } else if (sun == 11) {
        self.topLeftSquareContainer.innerImage.hidden = false;
    } else if (sun == 12) {
        self.leftTriangleContainer.innerImage.hidden = false;
    }
    
    [self toggleBorderForSymbol:mercury];
    [self toggleBorderForSymbol:venus];
    [self toggleBorderForSymbol:mars];
}


- (void) toggleBorderForSymbol: (int) number {
    if (number == 1) {
        [self.leftArrowContainer setBorderActive];
    } else if (number == 2) {
        [self.bottomLeftSquareContainer setBorderActive];
    } else if (number == 3) {
        [self.bottomTriangleContainer setBorderActive];
    } else if (number == 4) {
        [self.bottomArrowContainer setBorderActive];
    } else if (number == 5) {
        [self.bottomRightSquareContainer setBorderActive];
    } else if (number == 6) {
        [self.rightTriangleContainer setBorderActive];
    } else if (number == 7) {
        [self.rightArrowContainer setBorderActive];
    } else if (number == 8) {
        [self.topRightSquareContainer setBorderActive];
    } else if (number == 9) {
        [self.topTriangleContainer setBorderActive];
    } else if (number == 10) {
        [self.topArrowContainer setBorderActive];
    } else if (number == 11) {
        [self.topLeftSquareContainer setBorderActive];
    } else if (number == 12) {
        [self.leftTriangleContainer setBorderActive];
    }
}


- (void) toggleAllSymbols: (BOOL) isHidden {
    
     [self.topLeftSquareContainer toggleAllSubviewsHidden:isHidden];
     [self.topRightSquareContainer toggleAllSubviewsHidden:isHidden];
     [self.bottomRightSquareContainer toggleAllSubviewsHidden:isHidden];
     [self.bottomLeftSquareContainer
     toggleAllSubviewsHidden:isHidden];
     
     [self.topTriangleContainer toggleAllSubviewsHidden:isHidden];
     [self.rightTriangleContainer toggleAllSubviewsHidden:isHidden];
     [self.bottomTriangleContainer toggleAllSubviewsHidden:isHidden];
     [self.leftTriangleContainer
     toggleAllSubviewsHidden:isHidden];
     
     [self.topArrowContainer toggleAllSubviewsHidden:isHidden];
     [self.rightArrowContainer toggleAllSubviewsHidden:isHidden];
     [self.bottomArrowContainer toggleAllSubviewsHidden:isHidden];
     [self.leftArrowContainer
     toggleAllSubviewsHidden:isHidden];
}

@end
