//
//  SquareView.h
//  SunSing+
//
//  Created by Leonardo Amigoni on 2/17/16.
//  Copyright © 2016 Leonardo Amigoni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignGroup : UIView

@property (strong, nonatomic) NSString *shape;
@property (weak, nonatomic) IBOutlet UIImageView *innerImage;
@property (weak, nonatomic) IBOutlet UIImageView *middleImage;
@property (weak, nonatomic) IBOutlet UIImageView *outerImage;
@property (weak, nonatomic) IBOutlet UIImageView *outerImage2;

- (id) initWithColor: (NSString *) colorName;
- (void) changeColor: (NSString *) colorName;
- (void) filterImage: (UIImageView *)imageToFilter color: (UIColor *) color;
- (void) configureGroup: (BOOL) innerOn middleOn: (BOOL) middleOn outerOne: (BOOL) outerOn outer2On: (BOOL)outer2On;
- (void) setBorderActive;
- (void) toggleAllSubviewsHidden: (BOOL) isHidden;

@end
