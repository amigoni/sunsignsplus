//
//  SquareView.m
//  SunSing+
//
//  Created by Leonardo Amigoni on 2/17/16.
//  Copyright © 2016 Leonardo Amigoni. All rights reserved.
//

#define RedColor [UIColor colorWithRed:171/255.0f green:52/255.0f blue:40/255.0f alpha:1.0]
#define GreenColor [UIColor colorWithRed:108/255.0f green:202/255.0f blue:108/255.0f alpha:1.0]
#define YellowColor [UIColor colorWithRed:255/255.0f green:179/255.0f blue:0/255.0f alpha:1.0]
#define BlueColor [UIColor colorWithRed:59/255.0f green:142/255.0f blue:165/255.0f alpha:1.0]

#import "SignGroup.h"

@interface SignGroup()



@end


@implementation SignGroup

-(id) init {
    return [self initWithColor:@"white"];
}


-(id) initWithColor: (NSString *) colorName {
    if (self = [super init]) {
        [self changeColor:colorName];
    }
    return(self);
}


- (void) changeColor: (NSString *) colorName {
    [self setImageForImageView:self.innerImage color:colorName];
    [self setImageForImageView:self.middleImage color:colorName];
    [self setImageForImageView:self.outerImage color:colorName];
    [self setImageForImageView:self.outerImage2 color:colorName];
}

- (void) setImageForImageView: (UIImageView *)imageView color: (NSString *) color {
    NSString *imageName = [[NSString alloc]init];
    
    if ([self.shape  isEqual: @"square"]){
        imageName = @"Square";
    } else if ([self.shape  isEqual: @"triangle"]){
        imageName = @"Triangle";
    } else if ([self.shape  isEqual: @"arrow"]){
        imageName = @"Arrow";
    }
    
    if (imageView == self.innerImage) {
        imageName = [imageName stringByAppendingString:@"In"];
    } else if (imageView == self.middleImage) {
        imageName = [imageName stringByAppendingString:@"Middle"];
    } else if (imageView == self.outerImage) {
        imageName = [imageName stringByAppendingString:@"Out"];
    } else if (imageView == self.outerImage2) {
        imageName = [imageName stringByAppendingString:@"Out2"];
    }
    
    if ([color  isEqual: @"red"]) {
        imageName = [imageName stringByAppendingString:@"Red"];
    } else if ([color  isEqual: @"green"]) {
        imageName = [imageName stringByAppendingString:@"Green"];
    } else if ([color  isEqual: @"blue"]) {
        imageName = [imageName stringByAppendingString:@"Blue"];
    } else if ([color  isEqual: @"yellow"]) {
        imageName = [imageName stringByAppendingString:@"Yellow"];
    }
    
    imageName = [imageName stringByAppendingString:@".png"];
    
    [imageView setImage:[UIImage imageNamed:imageName]];
}


- (void) configureGroup: (BOOL) innerOn middleOn: (BOOL) middleOn outerOne: (BOOL) outerOn outer2On: (BOOL)outer2On{
    
    self.innerImage.hidden = true;
    self.middleImage.hidden = true;
    self.outerImage.hidden = true;
    self.outerImage2.hidden = true;
    
    if (innerOn) {
        self.innerImage.hidden = false;
    }
    
    if (middleOn) {
        self.middleImage.hidden = false;
    }
    
    if (outerOn) {
        self.outerImage.hidden = false;
    }
    
    if (outer2On) {
        self.outerImage2.hidden = false;
    }
}

- (void) constrainView {
    /*
    NSLayoutConstraint *horizonalContraints = NSLayoutConstraint(item: slider, attribute:
                                                 .LeadingMargin, relatedBy: .Equal, toItem: view,
                                                 attribute: .LeadingMargin, multiplier: 1.0,
                                                 constant: 20)

     */
}


- (void) setBorderActive {
    if (self.middleImage.hidden == true && self.outerImage.hidden == true) {
        self.middleImage.hidden = false;
        self.outerImage.hidden = true;
        self.outerImage2.hidden = true;
    } else if (self.middleImage.hidden == true && self.outerImage.hidden == false){
        self.middleImage.hidden = false;
        self.outerImage.hidden = false;
        self.outerImage2.hidden = true;
    } else if (self.middleImage.hidden == false && self.outerImage.hidden == false) {
        self.middleImage.hidden = false;
        self.outerImage.hidden = false;
        self.outerImage2.hidden = false;
    }
}


- (void) toggleAllSubviewsHidden: (BOOL) isHidden {
    self.innerImage.hidden = isHidden;
    self.middleImage.hidden = isHidden;
    self.outerImage.hidden = isHidden;
    self.outerImage2.hidden = isHidden;
}

@end
