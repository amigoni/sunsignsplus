//
//  ViewController.h
//  SunSing+
//
//  Created by Leonardo Amigoni on 2/17/16.
//  Copyright © 2016 Leonardo Amigoni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignDisplayController.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet SignDisplayController *signDisplayController;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *sunTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunSignLabel;
@property (weak, nonatomic) IBOutlet UILabel *mercuryTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mercurySignLabel;
@property (weak, nonatomic) IBOutlet UILabel *venusTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *venusSignLabel;
@property (weak, nonatomic) IBOutlet UILabel *marsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *marsSignLabel;


@end

