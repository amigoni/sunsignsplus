//
//  ViewController.m
//  SunSing+
//
//  Created by Leonardo Amigoni on 2/17/16.
//  Copyright © 2016 Leonardo Amigoni. All rights reserved.
//

// Colors

#define RedColor [UIColor colorWithRed:171/255.0f green:52/255.0f blue:40/255.0f alpha:1.0]
#define GreenColor [UIColor colorWithRed:108/255.0f green:202/255.0f blue:108/255.0f alpha:1.0]
#define YellowColor [UIColor colorWithRed:255/255.0f green:179/255.0f blue:0/255.0f alpha:1.0]
#define BlueColor [UIColor colorWithRed:59/255.0f green:142/255.0f blue:165/255.0f alpha:1.0]

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSArray *years;
    NSArray *months;
    NSDictionary *signData;
    NSDictionary *signTable;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    signData = [self loadDataFromFile:@"SignData.json"];
    signTable = [self loadDataFromFile:@"SignTable.json"];
    
    [self.datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
                                [UIDatePicker
                                 instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:self.datePicker];
    self.datePicker.subviews[0].subviews[1].backgroundColor = [UIColor clearColor];
    self.datePicker.subviews[0].subviews[2].backgroundColor = [UIColor clearColor];
    
    [self.signDisplayController toggleAllSymbols:false];
}


- (IBAction)onGoPressed:(id)sender {
    NSDate *pickerDate = [_datePicker date];
    double dateAsNumber = [pickerDate timeIntervalSince1970];
    
    int sun = [self signForPlanet:@"sun" dateToLookUp:dateAsNumber];
    int mercury = [self signForPlanet:@"mercury" dateToLookUp:dateAsNumber];
    int venus = [self signForPlanet:@"venus" dateToLookUp:dateAsNumber];
    int mars = [self signForPlanet:@"sun" dateToLookUp:dateAsNumber];
    
    [self setLabelColor:self.sunTitleLabel sign:sun];
    [self setLabelColor:self.sunSignLabel sign:sun];
    [self setLabelColor:self.mercuryTitleLabel sign:mercury];
    [self setLabelColor:self.mercurySignLabel sign:mercury];
    [self setLabelColor:self.venusTitleLabel sign:venus];
    [self setLabelColor:self.venusSignLabel sign:venus];
    [self setLabelColor:self.marsTitleLabel sign:mars];
    [self setLabelColor:self.marsSignLabel sign:mars];
    
    NSLog(@"Sun: %i, Mercury: %i, Venus: %i, Mars: %i",sun, mercury, venus, mars);
    
    [self.signDisplayController toggleAllSymbols:true];
    [self.signDisplayController configureWithPlanetSigns:sun mercury:mercury venus:venus mars:mars];
}


- (void) setLabelColor: (UILabel *) label sign:(int) sign {
    NSString *color = [signTable[[NSString stringWithFormat:@"%i", sign]] valueForKey: @"color"];
    
    UIColor *newColor;
    
    if ([color isEqualToString:@"red"]) {
        newColor = RedColor;
    } else if ([color isEqualToString:@"green"]) {
        newColor = GreenColor;
    } else if ([color isEqualToString:@"blue"]) {
        newColor = BlueColor;
    } else if ([color isEqualToString:@"yellow"]) {
        newColor = YellowColor;
    }
    
    label.textColor = newColor;
}


- (int) signForPlanet: (NSString *) planetName dateToLookUp:(double) date {
    NSArray *planetData = signData[planetName];
    int sign = 0; //0 is invalid
    
    int i = 0;
    for (NSDictionary *data in planetData) {
        if ([data[@"timeStamp"] longValue]>= (long) date) {
            NSDictionary*previousData = planetData[i-1];
            sign = [[previousData valueForKey: @"Sign Number"] intValue];
            break;
        }
        ++i;
    }
    
    return sign;
}


-(NSDictionary *) loadDataFromFile:(NSString*)fileLocation{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileLocation stringByDeletingPathExtension] ofType:[fileLocation pathExtension]];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    
    NSError* error;
    return [NSJSONSerialization JSONObjectWithData:data
                                                    options:kNilOptions
                                                      error:&error];
}

- (IBAction)pickerValueChanged:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIDatePicker *datePicker = (UIDatePicker *)sender;
        
        /*
        if ([self.datePicker.date compare:self.datePicker.maximumDate] == NSOrderedAscending) {
            datePicker.date = self.datePicker.maximumDate;
        }
         */
        
        
        if ([self.datePicker.date compare:self.datePicker.minimumDate] == NSOrderedAscending) {
            datePicker.date = self.datePicker.minimumDate;
        }
    });
}

@end
